#!/bin/bash

# install python 3.9
#sudo apt -y install software-properties-common
#sudo add-apt-repository -y ppa:deadsnakes/ppa 
sudo apt -y update
#sudo apt -y upgrade
sudo apt -y install python3
sudo apt -y install python3-pip
sudo apt -y install python3.8
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 1
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.8 2
sudo update-alternatives --auto python3
sudo cp /usr/lib/python3/dist-packages/apt_pkg.cpython-36m-x86_64-linux-gnu.so /usr/lib/python3/dist-packages/apt_pkg.so
sudo python3 -m pip install --upgrade pip

# install git
sudo apt -y install git

#install kubectl
sudo apt-get install -y ca-certificates curl
sudo apt-get install -y apt-transport-https
sudo mkdir /etc/apt/keyrings
sudo curl -fsSLo /etc/apt/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl

# install ansible
python3 -m pip install --user ansible
export PATH="$HOME/.local/bin:$PATH"
ansible --version
#

ws=(stage prod)

for wsi in ${ws[*]}
 do
  mkdir -p ~/kubespray-$wsi
  git clone https://github.com/kubernetes-sigs/kubespray ~/kubespray-$wsi

  cd ~/kubespray-$wsi
  rm -rf ./inventory/mycluster
  rm -rf ./.git
  rm -rf ./.gitignore
  sudo pip3 install -r requirements.txt
  cp -rfp ./inventory/sample ./inventory/mycluster
  mv ~/hosts-$wsi.yaml  ./inventory/mycluster/hosts.yaml

  SUPIP=$(cat ~/SUPIP-$wsi)
  echo $SUPIP

  ansible-playbook -i ./inventory/mycluster/hosts.yaml ./cluster.yml -b -v

  mkdir -p ~/.kube
  ssh -i ~/.ssh/id_rsa ubuntu@$SUPIP "sudo cat /root/.kube/config" > ~/.kube/config-$wsi

done
