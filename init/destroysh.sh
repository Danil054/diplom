#!/bin/bash

export YC_TOKEN=$(yc iam create-token)
export YC_CLOUD_ID=$(yc config get cloud-id)
export YC_FOLDER_ID=$(yc config get folder-id)

cd ../ter-first-create
export AWS_ACCESS_KEY_ID=$(terraform output -json s3-access-key | jq -j ".")
export AWS_SECRET_ACCESS_KEY=$(terraform output -json s3-secret-key | jq -j ".")

cd ../ter-second-create

ws=(stage prod)
for wsi in ${ws[*]}
do
 terraform workspace select $wsi
 terraform destroy -auto-approve
done

cd ../ter-first-create
 terraform destroy -auto-approve

cd ../ter-runner
  terraform destroy -auto-approve