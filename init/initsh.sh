#!/bin/bash

export YC_TOKEN=$(yc iam create-token)
export YC_CLOUD_ID=$(yc config get cloud-id)
export YC_FOLDER_ID=$(yc config get folder-id)

cd ../ter-first-create
terraform init
terraform apply -auto-approve

export AWS_ACCESS_KEY_ID=$(terraform output -json s3-access-key | jq -j ".")
export AWS_SECRET_ACCESS_KEY=$(terraform output -json s3-secret-key | jq -j ".")

OUTIPNATVM=$(terraform output -json external_ip_address_vm-nat  | jq -j ".")
echo $OUTIPNATVM

cd ../ter-second-create
terraform init

ws=(stage prod)

for wsi in ${ws[*]}
 do
  terraform workspace new $wsi
  terraform workspace select $wsi
  terraform apply -auto-approve

#  OUTIPS=$(terraform output -json external_ip_address_vms)
  INTIPS=$(terraform output -json internal_ip_address_vms)
  OUTIPS=$INTIPS
  IPCOUNT=$(echo $OUTIPS | jq length)
  cp ../init/hosts.yaml.tpl ../init/hosts-$wsi.yaml

  for (( i=0; i < $IPCOUNT; i++ ))
   do
    TMPOUTIP=$(echo $OUTIPS | jq -j ".[$i]")
    TMPINTIP=$(echo $INTIPS | jq -j ".[$i]")
    sed -i s/OUTIP$i/$TMPOUTIP/g ../init/hosts-$wsi.yaml
    sed -i s/INTIP$i/$TMPINTIP/g ../init/hosts-$wsi.yaml
  done

  SUPIP=$(echo $OUTIPS | jq -j ".[0]")
  ssh -o StrictHostKeyChecking=no -i ~/.ssh/id_rsa-yc ubuntu@$OUTIPNATVM "echo $SUPIP > /home/ubuntu/SUPIP-$wsi"
  scp -i ~/.ssh/id_rsa-yc ../init/hosts-$wsi.yaml  ubuntu@$OUTIPNATVM:/home/ubuntu/hosts-$wsi.yaml


done

cd ../init/
scp -i ~/.ssh/id_rsa-yc ~/.ssh/id_rsa-yc  ubuntu@$OUTIPNATVM:/home/ubuntu/.ssh/id_rsa
scp -i ~/.ssh/id_rsa-yc ./remotecmds.sh  ubuntu@$OUTIPNATVM:/home/ubuntu/remotecmds.sh
ssh -o StrictHostKeyChecking=no -i ~/.ssh/id_rsa-yc ubuntu@$OUTIPNATVM "sudo chmod 600 /home/ubuntu/.ssh/id_rsa"
ssh -o StrictHostKeyChecking=no -i ~/.ssh/id_rsa-yc ubuntu@$OUTIPNATVM "sudo chmod +x /home/ubuntu/remotecmds.sh"

ssh -o StrictHostKeyChecking=no -i ~/.ssh/id_rsa-yc -t ubuntu@$OUTIPNATVM "/home/ubuntu/remotecmds.sh"

