all:
  hosts:
    node1:
      ansible_ssh_private_key_file: ~/.ssh/id_rsa
      ansible_connection: ssh
      ansible_user: ubuntu
      ansible_host: OUTIP0
      ip: INTIP0
      access_ip: INTIP0
    node2:
      ansible_ssh_private_key_file: ~/.ssh/id_rsa
      ansible_connection: ssh
      ansible_user: ubuntu
      ansible_host: OUTIP1
      ip: INTIP1
      access_ip: INTIP1
    node3:
      ansible_ssh_private_key_file: ~/.ssh/id_rsa
      ansible_connection: ssh
      ansible_user: ubuntu
      ansible_host: OUTIP2
      ip: INTIP2
      access_ip: INTIP2
  children:
    kube_control_plane:
      hosts:
        node1:
    kube_node:
      hosts:
        node1:
        node2:
        node3:
    etcd:
      hosts:
        node1:
        node2:
        node3:
    k8s_cluster:
      children:
        kube_control_plane:
        kube_node:
    calico_rr:
      hosts: {}
