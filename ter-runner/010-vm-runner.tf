data "yandex_vpc_network" "vpcnet" {
  name = "vpc-network"
}

resource "yandex_vpc_subnet" "subnet-runners" {
  v4_cidr_blocks = ["192.168.100.0/24"]
  zone           = "ru-central1-a"
  network_id     = data.yandex_vpc_network.vpcnet.id
  name = "subnet-runners"
}

resource "yandex_compute_instance" "vm-runner" {
  name = "vm-runner"
  allow_stopping_for_update = true
  scheduling_policy {
    preemptible = true
  }

  platform_id = "standard-v1"

  resources {
    cores  = 2
    memory = 2
    core_fraction = 20
  }

  boot_disk {
    initialize_params {
      image_id = "fd8aeg00ca1t9obj3irl"
      size = 20
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.subnet-runners.id}"
    ip_address = "192.168.100.250"
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa-yc.pub")}"
  }
}

output "internal_ip_address_vm-runner" {
  value = yandex_compute_instance.vm-runner.network_interface.0.ip_address
}
output "nat_ip_address_vm-runner" {
  value = yandex_compute_instance.vm-runner.network_interface.0.nat_ip_address
}

