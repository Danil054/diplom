terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "dan054s3-bucket"
    region     = "ru-central1"
    key        = "stage/stage.tfstate"
#    access_key = "<идентификатор статического ключа>"
#    secret_key = "<секретный ключ>"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  zone = "ru-central1-a"       
#  token = ""     IN ENV YC_TOKEN
#  cloud_id = ""  IN ENV YC_CLOUD_ID
#  folder_id = "" IN ENV YC_FOLDER_ID
}


