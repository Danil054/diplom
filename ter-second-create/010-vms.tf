data "yandex_vpc_network" "vpcnet" {
  name = "vpc-network"
}

data "yandex_vpc_subnet" "subnet-prod" {
  name = "subnet-prod"
}

data "yandex_vpc_subnet" "subnet-stage" {
  name = "subnet-stage"
}


locals {
  ws = "${terraform.workspace}"

  zone-list = {
    "prod" = "ru-central1-a"
    "stage" = "ru-central1-b"
    "tst" = "ru-central1-b"
  }

  subnet-ids = {
    "prod" = data.yandex_vpc_subnet.subnet-prod.id
    "stage" = data.yandex_vpc_subnet.subnet-stage.id
    "tst" = data.yandex_vpc_subnet.subnet-stage.id
  }

 zone-name = "${lookup(local.zone-list,local.ws)}"
 sub-id = "${lookup(local.subnet-ids,local.ws)}"

}

resource "yandex_compute_instance" "vms" {
  
  count = 3
  name = "kub-${count.index}-${terraform.workspace}"
  zone = local.zone-name

  allow_stopping_for_update = true

  scheduling_policy {
    preemptible = false
  }
  
  platform_id = "standard-v1"

  resources {
    cores  = 2
    memory = 2
    core_fraction = 20
  }

  boot_disk {
    initialize_params {
      image_id = "fd8aeg00ca1t9obj3irl"
      size = 20
    }
  }

  network_interface {
    subnet_id = local.sub-id
    nat = false
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa-yc.pub")}"
  }
}

output "Env" {
  value = local.ws
}

output "internal_ip_address_vms" {
  value = yandex_compute_instance.vms[*].network_interface.0.ip_address
}

output "external_ip_address_vms" {
  value = yandex_compute_instance.vms[*].network_interface.0.nat_ip_address
}


