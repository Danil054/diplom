# Диплом Netology  

Диплом состоит из трёх  частей:  
- Подготовка Yandex облака  
- Создание инфраструктуры prod и stage  
- Разворот мониторинга инфраструктуры, разворот нашего приложения в инфраструктуру  

Инфраструктура представляет собой два кубернетес кластера, один для prod, второй для stage в Yandex облаке,  
и один сервер ("бастион") с внешним IP адресом.  

<details>
<summary><h2>Подготовка к развороту инфраструктуры.</h2></summary>  

### Подготовка облака:  
- В Yandex Cloud необходимо создать облако и каталог в нём  
- Получить [OAUTH токен](https://oauth.yandex.ru/authorize?response_type=token&client_id=1a6990aa636648e9b2ef855fa7bec2fb)  
- В каталоге создать сервисный аккаунт, с необходимым набором ролей для создания инфраструктуры (iam.serviceAccounts.admin,compute.admin,vpc.admin,storage.admin)  

### На компьютере, с которого будет запускаться разворот инфраструктуры:  
- Установить yc:  
```
curl -sSL https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
```
- Инициализировать yc:  
```
yc init
```
- Натсроить работу yc от имени сервисного аккаунта:  
```
yc iam key create --service-account-id "ajesi68n08cpoao58ant" --folder-name "netology" --output key.json
yc config profile create ter
yc config profile delete default
yc config set service-account-key key.json
yc config set cloud-id b1gut5n9lke8deijokgn
yc config set folder-id b1gqcqnmiduqna3uc4e6
```
- Создать и экспортировать токен, для работы terraform, в переменные окружения:  
```
export YC_TOKEN=$(yc iam create-token)
export YC_CLOUD_ID=$(yc config get cloud-id)
export YC_FOLDER_ID=$(yc config get folder-id)
```

- Установить Terraform и настроить работу с яндекс зеркалом:  
```
wget https://hashicorp-releases.yandexcloud.net/terraform/1.3.7/terraform_1.3.7_linux_amd64.zip
unzip terraform_1.3.7_linux_amd64.zip
chmod +x terraform
mv ./terraform /usr/local/bin/terraform

mv ~/.terraformrc ~/.terraformrc.old
vim ~/.terraformrc
added:
provider_installation {
  network_mirror {
    url = "https://terraform-mirror.yandexcloud.net/"
    include = ["registry.terraform.io/*/*"]
  }
  direct {
    exclude = ["registry.terraform.io/*/*"]
  }
}
```

- Установить утилиту jq (для удобного вычитывания переменных из tfstate):  
```
sudo apt -y install jq
```
</details>

<details>
<summary><h2>Разворот инфраструктуры.</h2></summary>  

- Склонировать данный репозиторий, и перейти в каталог init:  
```
git clone https://gitlab.com/Danil054/diplom.git
cd ./diplom/init
```
- Запустить [init](https://gitlab.com/Danil054/diplom/-/blob/main/init/initsh.sh) скрипт:  
```
./initsh.sh
```
Данный скрипт, при помощи Terraform и подготовленных манифестов:  
[ter-first-create](https://gitlab.com/Danil054/diplom/-/tree/main/ter-first-create)  
[ter-second-create](https://gitlab.com/Danil054/diplom/-/tree/main/ter-second-create)  
Первым этапом создаёт в каталоге Яндекс облака:  
VPC: vpc-network  
Таблицу маршрутизации: route-to-nat-vm  
Подсети: subnet-stage, subnet-prod, subnet-public  
Виртуальный сервер ("бастион"): vm-nat (с внутренним и внешним IP адресом)  
Статический ключ доступа к S3 для сервисного аккаунта: serviceacc1-static-key  
S3 бакет для дальнейшего хранения tfstate: dan054s3-bucket  
Экспортирует AWS_ACCESS_KEY_ID и AWS_SECRET_ACCESS_KEY в переменные окружения  
Создаёт переменную $OUTIPNATVM (в которой содержится внешний IP адрес сервера vm-nat)  

Вторым этапом:  
Создаёт два workspace в Terraform stage и prod  
Создаёт сервера для окружений stage и prod  
На основе шаблонного файла [hosts.yaml.tpl](https://gitlab.com/Danil054/diplom/-/blob/main/init/hosts.yaml.tpl),  
формирует файлы hosts-prod.yaml и hosts-stage.yaml для дальнейшего их использования в качестве inventory Kubespray  
Копирует необходимые файлы на сервер "бастион" (vm-nat)  
Запускает на сервере (vm-nat) скрипт [remotecmds.sh](https://gitlab.com/Danil054/diplom/-/blob/main/init/remotecmds.sh),  
он запускает подготовку к установке kubernetes (устанавливает git, kubectl и ansible), затем через ansible (Kubespray) устанвливает кубернетес кластера, соответственно на серверах для stage и серверах для prod  
В результате получится два кубернетес кластера, конфигурации для подключения к кластерам сохраняются на сервере "бастион" (vm-nat) в файлах:  
```
~/.kube/config-stage
~/.kube/config-prod
```
Для дальнейшего обслуживания кластеров кубернетес, через ansible (Kubespray), на сервере бостион (vm-nat) присутствуют каталоги: 
```
~/kubespray-prod
~/kubespray-stage
```

Для удобства, можно объединить, полученные файлы конфигурации подключения к кластерам kubernetes в один, разбив кластера для stage и prod по контекстам.  
Пример [конфига для подключения](https://gitlab.com/Danil054/diplom/-/blob/main/init/kube-config.example)  
В конфигурации два контекста:  
- clsprodctx (подключение к кластеру prod)  
- clsstagectx (подключение к кластеру stage)  
Данный (объединённый) файл помещаем в ~/.kube/config  

Для быстрого переключения между контекстами создаём алиасы:  
```
alias topr='kubectl config use-context clsprodctx'
alias tost='kubectl config use-context clsstagectx'
```
На данном этапе, с сервера "бастион" (vm-nat) можно работать с кластерами stage и prod,  
проверим состояние нод:  
```
ubuntu@fhmkvabaceed88q4ntd9:~$
ubuntu@fhmkvabaceed88q4ntd9:~$ topr
Switched to context "clsprodctx".
ubuntu@fhmkvabaceed88q4ntd9:~$
ubuntu@fhmkvabaceed88q4ntd9:~$  kubectl get nodes -o wide
NAME    STATUS   ROLES           AGE   VERSION   INTERNAL-IP     EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION       CONTAINER-RUNTIME
node1   Ready    control-plane   13d   v1.25.6   192.168.10.33   <none>        Ubuntu 18.04.6 LTS   4.15.0-112-generic   containerd://1.6.15
node2   Ready    <none>          13d   v1.25.6   192.168.10.34   <none>        Ubuntu 18.04.6 LTS   4.15.0-112-generic   containerd://1.6.15
node3   Ready    <none>          13d   v1.25.6   192.168.10.24   <none>        Ubuntu 18.04.6 LTS   4.15.0-112-generic   containerd://1.6.15
ubuntu@fhmkvabaceed88q4ntd9:~$
ubuntu@fhmkvabaceed88q4ntd9:~$ tost
Switched to context "clsstagectx".
ubuntu@fhmkvabaceed88q4ntd9:~$
ubuntu@fhmkvabaceed88q4ntd9:~$ kubectl get nodes -o wide
NAME    STATUS   ROLES           AGE   VERSION   INTERNAL-IP     EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION       CONTAINER-RUNTIME
node1   Ready    control-plane   13d   v1.25.6   192.168.11.9    <none>        Ubuntu 18.04.6 LTS   4.15.0-112-generic   containerd://1.6.15
node2   Ready    <none>          13d   v1.25.6   192.168.11.23   <none>        Ubuntu 18.04.6 LTS   4.15.0-112-generic   containerd://1.6.15
node3   Ready    <none>          13d   v1.25.6   192.168.11.24   <none>        Ubuntu 18.04.6 LTS   4.15.0-112-generic   containerd://1.6.15
ubuntu@fhmkvabaceed88q4ntd9:~$

```

</details>

<details>
<summary><h2>Работа с kubernetes.</h2></summary>  


Первоначально предполагалась следующая схема работы:  
- установить Nginx ingress controller с типом LoadBalancer (controller.service.type=LoadBalancer)  
- для выдачи EXTERNAL-IP адреса контроллеру, установить MetalLB load-balancer, который выдаст IP из тогоже сетевого диапазона, что и ноды кластера  
- на сервере "бастион" (vm-nat) установить haproxy, который будет проксировать трафик с Внешнего IP адреса, на выданный (через MetalLB) IP адрес ingress контроллеру  
- таким образом будет задействован только один внешний IP адрес на vm-nat сервере  

Но из переписки с тех. поддержкой yandex-а, было выяснено, что данную схему нельзя реализовать на их облаке, так как MetalLB будет выделять (неучтённый) виртуальный ip адрес, к которому не будет доступа из vm-nat сервера.  
В яндекс облаке нельзя работать с виртуальными ip адресами, так как у них нет прямой L2 связности между серверами, даже если они принадлежат одной и тойже подсети.  

Было решено:
- использовать Nginx ingress controller с типом NodePort и "прибитыми гвоздями" номерами node портов (30080,30443)  
- не использовать MetalLB  
- на сервере "бастион" (vm-nat) установить haproxy, который будет проксировать трафик с Внешнего IP адреса, на IP адреса всех нод кластера (с учётом prod и stage кластеров)  

На сервер "бастион" (vm-nat) установлен haproxy, со следующей конфигурацией:
[пример конфигурации для haproxy](https://gitlab.com/Danil054/diplom/-/blob/main/init/haproxy.cfg.example)  

А также установлен Helm:  
```
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
```

Установка Nginx ingress controller в кластерах prod и stage:  
```
topr
helm upgrade --install ingress-nginx ingress-nginx --repo https://kubernetes.github.io/ingress-nginx --namespace ingress-nginx --create-namespace --set controller.service.type=NodePort --set controller.service.nodePorts.https=30443 --set controller.service.nodePorts.http=30080

tost
helm upgrade --install ingress-nginx ingress-nginx --repo https://kubernetes.github.io/ingress-nginx --namespace ingress-nginx --create-namespace --set controller.service.type=NodePort --set controller.service.nodePorts.https=30443 --set controller.service.nodePorts.http=30080

```

Установка мониторинга в кластерах prod и stage:  
```
# https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update

topr
helm install monstack  prometheus-community/kube-prometheus-stack --namespace monitoring --create-namespace

tost
helm install monstack  prometheus-community/kube-prometheus-stack --namespace monitoring --create-namespace
```

Для доступа "извне" к Grafana в кластерах prod и stage применим соответствующие манифесты:  
- [ingress-grf-prod.yaml](https://gitlab.com/Danil054/appdeploy/-/blob/main/monitoring-kub/ingress-grf-prod.yaml)  
- [ingress-grf-stage.yaml](https://gitlab.com/Danil054/appdeploy/-/blob/main/monitoring-kub/ingress-grf-stage.yaml)  
```
# add ingres for grafana
topr
kubectl apply -f ingress-grf-prod.yaml

tost
kubectl apply -f ingress-grf-stage.yaml
```
Теперь Grafana в prod кластере доступна по адресу:  
[http://monprod.adminia.ru](http://monprod.adminia.ru)

Grafana в stage кластере доступна по адресу:  
[http://monstage.adminia.ru](http://monstage.adminia.ru)

(предварительно в DNS зону adminia.ru были добавлены А записи для monprod.adminia.ru и monstage.adminia.ru, указывающие на внешний IP  адрес нашего сервера "бастион" vm-nat)

Приложение подготовлено в отдельном репозитории:  
[appdeploy](https://gitlab.com/Danil054/appdeploy/-/tree/main)  
Образы приложения хранятся на docker hub:  
[dan054n3/app](https://hub.docker.com/r/dan054n3/app/tags)  

Для установки приложения в кубернетес кластер, подготовлен helm чарт:  
[helm app](https://gitlab.com/Danil054/appdeploy/-/tree/main/app-helm/app)

В корне репозитория с приложением создан файл [.gitlab-ci.yml](https://gitlab.com/Danil054/appdeploy/-/blob/main/.gitlab-ci.yml) для организации CI/CD  
Pipeline состоит из 4-х этапов:  
- сборка образа с приложением, и отправка (push) его в репозиторий docker hub (при любом коммите)  
- push тага latest (при любом коммите ветки main), и номера тага (если коммит в гит тагирован)  
- деплой приложения в stage кластер (при любом коммите)  
- деплой приложения в prod кластер (при тагировании коммита)  

Файл конфигурации подключения к кластерам кубернетес хранится в настройках CI/CD в Secure Files.  

После коммита и/или проставления тага, выполнится [pipeline](https://gitlab.com/Danil054/appdeploy/-/pipelines)  
Приложение автоматически соберётся, запушится в docker hub, и установится в кубернетес кластере stage (если не тагирован коммит) и в кластерах stage и prod, если тагирован коммит.  

Приложение в prod доступно по адресу:  
[http://srvprod.adminia.ru/](http://srvprod.adminia.ru/)  

Приложение в stage доступно по адресу:  
[http://srvstage.adminia.ru/](http://srvstage.adminia.ru/)  

(предварительно в DNS зону adminia.ru были добавлены А записи для srvprod.adminia.ru и srvstage.adminia.ru, указывающие на внешний IP  адрес нашего сервера "бастион" vm-nat)  
</details>

<details>
<summary><h2>Скриншоты работы приложения.</h2></summary>  
![Приложение в prod кластере](https://gitlab.com/Danil054/appdeploy/-/blob/main/pics/appprod.png)  
![Приложение в stage кластере](https://gitlab.com/Danil054/appdeploy/-/blob/main/pics/appstage.png)  
![Grafana в prod кластере](https://gitlab.com/Danil054/appdeploy/-/blob/main/pics/grfprod.png)  
![Grafana в stage кластере](https://gitlab.com/Danil054/appdeploy/-/blob/main/pics/grfstage.png)  

</details>