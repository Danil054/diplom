resource "yandex_vpc_network" "vpcnet" {
  name = "vpc-network"
}


resource "yandex_vpc_route_table" "route-to-nat-vm" {
  network_id = "${yandex_vpc_network.vpcnet.id}"
  name = "route-to-nat-vm"

  static_route {
    destination_prefix = "0.0.0.0/0"
    next_hop_address   = "${yandex_compute_instance.vm-nat.network_interface.0.ip_address}"
  }
}

resource "yandex_vpc_subnet" "subnet-public" {
  v4_cidr_blocks = ["192.168.0.0/24"]
  zone           = "ru-central1-a"
  network_id     = "${yandex_vpc_network.vpcnet.id}"
  name = "subnet-public"
}

resource "yandex_vpc_subnet" "subnet-prod" {
  v4_cidr_blocks = ["192.168.10.0/24"]
  zone           = "ru-central1-a"
  network_id     = "${yandex_vpc_network.vpcnet.id}"
  name = "subnet-prod"
  route_table_id = "${yandex_vpc_route_table.route-to-nat-vm.id}"
}

resource "yandex_vpc_subnet" "subnet-stage" {
  v4_cidr_blocks = ["192.168.11.0/24"]
  zone           = "ru-central1-b"
  network_id     = "${yandex_vpc_network.vpcnet.id}"
  name = "subnet-stage"
  route_table_id = "${yandex_vpc_route_table.route-to-nat-vm.id}"
}



