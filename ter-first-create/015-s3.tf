locals {
  svcacc_id = "ajesi68n08cpoao58ant" #sa-ter
}


# Create Static Access Keys
resource "yandex_iam_service_account_static_access_key" "serviceacc1-static-key" {
  service_account_id = local.svcacc_id
  description        = "static access key for object storage"
}

output "s3-access-key" {
  value = yandex_iam_service_account_static_access_key.serviceacc1-static-key.access_key
  sensitive = true
}

output "s3-secret-key" {
  value = yandex_iam_service_account_static_access_key.serviceacc1-static-key.secret_key
  sensitive = true
}


# Use keys to create bucket
resource "yandex_storage_bucket" "dan054s3-bk" {
  access_key = yandex_iam_service_account_static_access_key.serviceacc1-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.serviceacc1-static-key.secret_key
  bucket = "dan054s3-bucket"
  force_destroy = true

}



