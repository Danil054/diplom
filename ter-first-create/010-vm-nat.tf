resource "yandex_compute_instance" "vm-nat" {
  name = "vm-nat"
  allow_stopping_for_update = true
  scheduling_policy {
    preemptible = false
  }

  platform_id = "standard-v1"

  resources {
    cores  = 2
    memory = 2
    core_fraction = 20
  }

  boot_disk {
    initialize_params {
      image_id = "fd8o8aph4t4pdisf1fio"
      size = 20
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-public.id
    ip_address = "192.168.0.254"
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa-yc.pub")}"
  }
}

output "internal_ip_address_vm-nat" {
  value = yandex_compute_instance.vm-nat.network_interface.0.ip_address
}

output "external_ip_address_vm-nat" {
  value = yandex_compute_instance.vm-nat.network_interface.0.nat_ip_address
}
